package com.sakura;

import com.sakura.config.JavaConfig;
import com.sakura.service.UserService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {


    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(JavaConfig.class);
        applicationContext.start();
        UserService userService = (UserService) applicationContext.getBean("userService");
        userService.query();

    }
}
